package dataLayer;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Order;

import java.io.FileWriter;
import java.util.ArrayList;

public class BillGenerator {

    public static void printBill(Order order, ArrayList<MenuItem> orderedItems) {
        try {
            FileWriter myWriter = new FileWriter("bill-order" + order.getId() + ".txt");
            myWriter.write("Order with id " + order.getId() + " was successfully processed.\n");
            myWriter.write("Order table: " + order.getTable() + "\n");
            myWriter.write("Order details:\n");
            myWriter.write("---------------------------------------------------------------------------\n");
            double totalCost = 0;
            for (MenuItem menuItem: orderedItems) {
                totalCost += menuItem.computePrice();
                if (menuItem instanceof CompositeProduct)
                    myWriter.write(menuItem.getName() + " - " + menuItem.getPrice() + "euro - " + ((CompositeProduct) menuItem).getIngredients().toString() + "\n");
                else if(menuItem instanceof BaseProduct)
                    myWriter.write(menuItem.getName() + " - " + menuItem.getPrice() + "euro\n");
            }
            myWriter.write("---------------------------------------------------------------------------\nTotal cost = " + totalCost + "\n---------------------------------------------------------------------------\nHave a nice day!");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (Exception e) {
            System.out.println("An error occured!");
        }
    }
}
