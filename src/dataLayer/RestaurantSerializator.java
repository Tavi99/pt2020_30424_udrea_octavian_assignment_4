package dataLayer;

import businessLayer.Restaurant;

import java.io.*;

public class RestaurantSerializator {

    public static void serializeRestaurant(Restaurant restaurant, String serializatorFileName) {
        try {

            FileOutputStream file = new FileOutputStream(serializatorFileName);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(restaurant);
            out.close();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Restaurant deserializeRestaurant(String serializatorFileName) {
        Restaurant restaurant = null;

        try {
            FileInputStream file = new FileInputStream(serializatorFileName);
            ObjectInputStream in = new ObjectInputStream(file);

            restaurant = (Restaurant)in.readObject();
            file.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return restaurant;
    }
}
