import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;
import presentationLayer.AdministratorGraphicalUserInterface;
import presentationLayer.WaiterGraphicalUserInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class StartApp {

    public static void main(String[] args) {

        ArrayList<MenuItem> restaurantMenu = new ArrayList<>();
        HashMap<Order, ArrayList<MenuItem>> restaurantOrders = new HashMap<>();
        Restaurant restaurant = new Restaurant(restaurantMenu, restaurantOrders, 0);

        restaurant = RestaurantSerializator.deserializeRestaurant(args[0]);

        AdministratorGraphicalUserInterface administratorGUI = new AdministratorGraphicalUserInterface(restaurant);
        WaiterGraphicalUserInterface waiterGUI = new WaiterGraphicalUserInterface(restaurant);

        /**try {
            TimeUnit.SECONDS.sleep(60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        RestaurantSerializator.serializeRestaurant(restaurant, args[0]);
    }
}
