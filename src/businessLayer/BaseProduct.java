package businessLayer;

public class BaseProduct extends MenuItem{

    public double computePrice() {
        return this.getPrice();
    }

    public String toString() {
        return (this.getName() + " " + this.getPrice());
    }
}
