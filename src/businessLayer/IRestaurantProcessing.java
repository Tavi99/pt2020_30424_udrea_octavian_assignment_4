package businessLayer;

import java.util.ArrayList;

public interface IRestaurantProcessing {

    /**
     * adds a new MenuItem object into restaurant menu
     * @param menuItem the MenuItem object which will be added into menu
     */
    void createNewMenuItem(MenuItem menuItem);

    /**
     * removes a MenuItem object from restaurant menu which has the specified index as parameter
     * @param menuItemIndex the MenuItem index which will be removed from menu
     */
    void deleteMenuItem(int menuItemIndex);

    /**
     * modifies the price of an MenuItem object
     * @param menuItem the MenuItem object whose price will be modified
     * @param newPrice the new price of the MenuItem object
     */
    void editMenuItem(MenuItem menuItem, double newPrice);

    /**
     * adds a new order, and also the items which compose it into restaurant orders
     * @param order the order which will be added into restaurant orders list
     * @param items the items corresponding to the order
     */
    void createNewOrder(Order order, ArrayList<MenuItem> items);

    /**
     * computes the total price for a list of MenuItem objects
     * @param items the list of the MenuItem objects
     * @return a double value which represents the total price of the items from the list
     */
    double computePriceForOrder(ArrayList<MenuItem> items);

    /**
     * generates a .txt file which represent a bill for an order
     * @param order the order for which the bill is generated
     * @param orderedItems the ordered items which compose the order
     */
    void generateBill(Order order, ArrayList<MenuItem> orderedItems);
}
