package businessLayer;

import dataLayer.BillGenerator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

    /**
     * the restaurant menu
     */
    private ArrayList<MenuItem> restaurantMenu;
    /**
     * the restaurant orders, each with its corresponding ordered items
     * the key is represented by the order, and the value is represented by the ordered items
     */
    private HashMap<Order, ArrayList<MenuItem>> restaurantOrders;
    /**
     * the items that are added in the current order
     */
    private ArrayList<MenuItem> currentOrderItems;
    /**
     * the orders without their corresponding items
     */
    private ArrayList<Order> ordersArray;
    /**
     * this field helps at generating unique order id
     */
    private int orderCounter;

    /**
     * constructor
     */
    public Restaurant(ArrayList<MenuItem> restaurantMenu, HashMap<Order, ArrayList<MenuItem>> restaurantOrders, int orderCounter) {
        this.restaurantMenu = restaurantMenu;
        this.restaurantOrders = restaurantOrders;
        this.orderCounter = orderCounter;
    }

    /**
     *
     * geters and seters for the instance variables
     */
    public ArrayList<Order> getOrdersArray() {
        return ordersArray;
    }

    public void setOrdersArray(ArrayList<Order> ordersArray) {
        this.ordersArray = ordersArray;
    }

    public ArrayList<MenuItem> getRestaurantMenu() {
        return restaurantMenu;
    }

    public void setRestaurantMenu(ArrayList<MenuItem> restaurantMenu) {
        this.restaurantMenu = restaurantMenu;
    }

    public HashMap<Order, ArrayList<MenuItem>> getRestaurantOrders() {
        return restaurantOrders;
    }

    public void setRestaurantOrders(HashMap<Order, ArrayList<MenuItem>> restaurantOrders) {
        this.restaurantOrders = restaurantOrders;
    }

    public ArrayList<MenuItem> getCurrentOrderItems() {
        return currentOrderItems;
    }

    public void setCurrentOrderItems(ArrayList<MenuItem> currentOrderItems) {
        this.currentOrderItems = currentOrderItems;
    }

    public int getOrderCounter() {
        return orderCounter;
    }

    public void setOrderCounter(int orderCounter) {
        this.orderCounter = orderCounter;
    }


    /**
     * adds a new MenuItem object into restaurant menu
     * @param menuItem the MenuItem object which will be added into menu
     */
    @Override
    public void createNewMenuItem(MenuItem menuItem) {
        restaurantMenu.add(menuItem);
    }

    /**
     * removes a MenuItem object from restaurant menu which has the specified index as parameter
     * @param menuItemIndex the MenuItem index which will be removed from menu
     */
    @Override
    public void deleteMenuItem(int menuItemIndex) {
        restaurantMenu.remove(menuItemIndex);
    }

    /**
     * modifies the price of an MenuItem object
     * @param menuItem the MenuItem object whose price will be modified
     * @param newPrice the new price of the MenuItem object
     */
    @Override
    public void editMenuItem(MenuItem menuItem, double newPrice) {
        int menuItemIndex = getMenuItemIndex(menuItem);
        if (menuItemIndex >= 0)
            restaurantMenu.get(menuItemIndex).setPrice(newPrice);
    }

    /**
     * adds a new order, and also the items which compose it into restaurant orders
     * @param order the order which will be added into restaurant orders list
     * @param items the items corresponding to the order
     */
    @Override
    public void createNewOrder(Order order, ArrayList<MenuItem> items) {
        restaurantOrders.put(order, items);
    }

    /**
     * computes the total price for a list of MenuItem objects
     * @param items the list of the MenuItem objects
     * @return a double value which represents the total price of the items from the list
     */
    @Override
    public double computePriceForOrder(ArrayList<MenuItem> items) {
        double totalPrice = 0;
        ArrayList<MenuItem> orderedItems = items;

        for(MenuItem menuItem: orderedItems)
            totalPrice += menuItem.computePrice();
        return totalPrice;
    }

    /**
     * generates a .txt file which represent a bill for an order
     * @param order the order for which the bill is generated
     * @param orderedItems the ordered items which compose the order
     */
    @Override
    public void generateBill(Order order, ArrayList<MenuItem> orderedItems) {
        BillGenerator.printBill(order, orderedItems);
    }

    /**
     * returns the index of an MenuItem object which is being searched into the restaurant menu
     * @param menuItem the MenuItem object being searched
     * @return the index of the MenuItem if this is found, otherwise -1
     */
    public int getMenuItemIndex(MenuItem menuItem) {

        boolean check = restaurantMenu.contains(menuItem);

        if (check == true)
            return restaurantMenu.indexOf(menuItem);
        else
            return -1;
    }

    /**
     * searches a MenuItem object into restaurant menu by name
     * @param name the name of the MenuItem object which is being searched for
     * @return the object if it is find, otherwise null
     */
    public MenuItem searchItemByName(String name) {
        for (MenuItem menuItem: this.restaurantMenu) {
            if (menuItem.getName().equals(name)) {
                return menuItem;
            }
        }
        return null;
    }

    /**
     * adds a new MenuItem object into current order list of items
     * @param menuItem the MenuItem object which will be added
     */
    public void putMenuItemToCurrentOrder(MenuItem menuItem) {
        currentOrderItems.add(menuItem);
    }

    /**
     * removes a MenuItem object into current order list of items
     * @param menuItem the MenuItem object which will be removed
     */
    public void removeMenuItemFromCurrentOrder(MenuItem menuItem) {
        currentOrderItems.remove(menuItem);
    }

    /**
     * searches a MenuItem object which with a specified name in the current order list of items
     * @param name the name of the MenuItem object which is being searched for
     * @return the MenuItem object if it is found, otherwise null
     */
    public MenuItem searchItemByNameInCurrentOrder(String name) {
        for (MenuItem currentOrderItem: this.currentOrderItems) {
            if (currentOrderItem.getName().equals(name)) {
                return currentOrderItem;
            }
        }
        return null;
    }

    /**
     * computes the total cost of a list of MenuItem objects
     * @param orderedItems the MenuItem objects list
     * @return an integer which represents the total cost of the list
     */
    public double totalOrderCost(ArrayList<MenuItem> orderedItems) {
        double totalCost = 0;
        for (MenuItem orderItem: orderedItems) {
            totalCost += orderItem.computePrice();
        }
        return totalCost;
    }
}
