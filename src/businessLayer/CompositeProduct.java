package businessLayer;

import java.util.ArrayList;
import java.util.Objects;

public class CompositeProduct extends MenuItem{

    private ArrayList<MenuItem> ingredients;

    public CompositeProduct() {
        ingredients = new ArrayList<>();
    }

    public ArrayList<MenuItem> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<MenuItem> ingredients) {
        this.ingredients = ingredients;
    }

    public double computePrice() {

        double totalPrice = 0;

        for(MenuItem item: ingredients)
            totalPrice += item.computePrice();

        return totalPrice;
    }

    public void addIngredient(MenuItem item) {
        ingredients.add(item);
    }

    public void removeIngredient(MenuItem item) {
        ingredients.remove(item);
    }

    public String toString() {
        return (this.getName() + " " + this.getPrice() + " " + this.getIngredients());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompositeProduct)) return false;
        if (!super.equals(o)) return false;
        CompositeProduct that = (CompositeProduct) o;
        return Objects.equals(getIngredients(), that.getIngredients());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getIngredients());
    }
}
