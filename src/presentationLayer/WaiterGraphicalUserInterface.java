package presentationLayer;

import businessLayer.*;
import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class WaiterGraphicalUserInterface {

    private Restaurant restaurant;

    private JFrame frame = new JFrame("Waiter Window");

    private JPanel centralPanel = new JPanel();
    private JPanel secondaryPanel = new JPanel();
    private JPanel buttonsPanel = new JPanel();

    private JTextField menuItemTextField = new JTextField();
    private JTextField tableNumberTextField = new JTextField();

    private JLabel menuItemLabel = new JLabel("Menu Item: ");
    private JLabel tableNumberLabel = new JLabel("Table number: ");
    private JLabel currentOrderLabel = new JLabel("Current Order Menu Items");
    private JLabel allOrdersLabel = new JLabel("All Orders");

    private JButton addMenuItemToBillButton = new JButton("Add MenuItem to Order");
    private JButton removeMenuItemFromBillButton = new JButton("Remove Item from Order");
    private JButton totalPriceForOrderButton = new JButton("Total Price For Order");
    private JButton printBillButton = new JButton("Print Bill");
    private JButton currentOrderMenuItemsTableButton = new JButton("Show current order items");
    private JButton allOrdersTableButton = new JButton("Show all orders");
    private JButton addCurrentOrderButton = new JButton("Add current order");

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public WaiterGraphicalUserInterface(Restaurant restaurant) {

        menuItemLabel.setHorizontalAlignment(JLabel.CENTER);
        tableNumberLabel.setHorizontalAlignment(JLabel.CENTER);
        menuItemLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        tableNumberLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        currentOrderLabel.setHorizontalAlignment(JLabel.CENTER);
        allOrdersLabel.setHorizontalAlignment(JLabel.CENTER);
        currentOrderLabel.setFont(new Font("Arial", Font.PLAIN, 36));
        allOrdersLabel.setFont(new Font("Arial", Font.PLAIN, 36));

        menuItemTextField.setHorizontalAlignment(JTextField.CENTER);
        tableNumberTextField.setHorizontalAlignment(JTextField.CENTER);
        menuItemTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        tableNumberTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        menuItemTextField.setPreferredSize(new Dimension(50, 20));
        menuItemTextField.setEditable(true);
        tableNumberTextField.setPreferredSize(new Dimension(50, 20));
        tableNumberTextField.setEditable(true);

        addMenuItemToBillButton.setFont(new Font("Arial", Font.PLAIN, 9));
        removeMenuItemFromBillButton.setFont(new Font("Arial", Font.PLAIN, 9));
        totalPriceForOrderButton.setFont(new Font("Arial", Font.PLAIN, 12));
        printBillButton.setFont(new Font("Arial", Font.PLAIN, 12));
        currentOrderMenuItemsTableButton.setFont(new Font("Arial", Font.PLAIN, 12));
        allOrdersTableButton.setFont(new Font("Arial", Font.PLAIN, 12));
        addCurrentOrderButton.setFont(new Font("Arial", Font.PLAIN, 12));

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(600, 150);
        frame.setLocationRelativeTo(null);

        centralPanel.setLayout(new GridLayout(2, 1));
        centralPanel.setBackground(Color.WHITE);
        centralPanel.setVisible(true);
        secondaryPanel.setLayout(new GridLayout(2, 4));
        buttonsPanel.setLayout(new FlowLayout());

        addMenuItemToBillButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addMenuItemToBillActionPerformed(evt);
            }
        });

        removeMenuItemFromBillButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeMenuItemFromBillActionPerformed(evt);
            }
        });

        totalPriceForOrderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalPriceForOrderActionPerformed(evt);
            }
        });

        printBillButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printBillActionPerformed(evt);
            }
        });

        currentOrderMenuItemsTableButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                currentOrderMenuItemsTableActionPerformed(evt);
            }
        });

        allOrdersTableButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allOrdersTableActionPerformed(evt);
            }
        });

        addCurrentOrderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCurrentOrderActionPerformed(evt);
            }
        });

        secondaryPanel.add(menuItemLabel);
        secondaryPanel.add(menuItemTextField);
        secondaryPanel.add(tableNumberLabel);
        secondaryPanel.add(tableNumberTextField);
        secondaryPanel.add(addMenuItemToBillButton);
        secondaryPanel.add(removeMenuItemFromBillButton);
        secondaryPanel.add(totalPriceForOrderButton);
        secondaryPanel.add(printBillButton);
        buttonsPanel.add(addCurrentOrderButton);
        buttonsPanel.add(currentOrderMenuItemsTableButton);
        buttonsPanel.add(allOrdersTableButton);

        centralPanel.add(secondaryPanel);
        centralPanel.add(buttonsPanel);

        frame.add(centralPanel);
        frame.setResizable(false);
        frame.setVisible(true);

        this.restaurant = restaurant;
        this.restaurant.setCurrentOrderItems(new ArrayList<>());
        this.restaurant.setOrdersArray(new ArrayList<>());
    }

    private void addMenuItemToBillActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem menuItem = new MenuItem();
            menuItem.setName(menuItemTextField.getText());

            MenuItem itemFound = this.restaurant.searchItemByName(menuItem.getName());
            if (itemFound != null) {
                this.restaurant.putMenuItemToCurrentOrder(itemFound);
            }
            else
                JOptionPane.showMessageDialog(null, "Menu item does not exist in menu!", "Menu item info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void removeMenuItemFromBillActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem menuItem = new MenuItem();
            menuItem.setName(menuItemTextField.getText());
            MenuItem itemFound = this.restaurant.searchItemByNameInCurrentOrder(menuItem.getName());

            if (itemFound != null) {
                this.getRestaurant().removeMenuItemFromCurrentOrder(itemFound);
            }
            else
                JOptionPane.showMessageDialog(null, "Product does not exist in order!", "Product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required product name", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void totalPriceForOrderActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if ( this.getRestaurant().getCurrentOrderItems().isEmpty() )
                JOptionPane.showMessageDialog(null, "No ordered items in order to compute the total cost -> cost = 0", "No ordered items", JOptionPane.INFORMATION_MESSAGE);
            else {
                double totalCost = this.getRestaurant().computePriceForOrder(this.getRestaurant().getCurrentOrderItems());
                JOptionPane.showMessageDialog(null, "Total cost of this order = " + totalCost, "Total Cost", JOptionPane.INFORMATION_MESSAGE);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Some error occured", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void printBillActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            Order lastOrder = this.getRestaurant().getOrdersArray().get(this.getRestaurant().getOrdersArray().size() - 1);
            this.getRestaurant().generateBill(lastOrder, this.getRestaurant().getCurrentOrderItems());
            this.getRestaurant().setCurrentOrderItems(new ArrayList<>());
            JOptionPane.showMessageDialog(null, "Bill printed", "Bill", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {

        }
    }

    private void currentOrderMenuItemsTableActionPerformed(java.awt.event.ActionEvent evt) {
        JFrame menuFrame = new JFrame("Current Order Items");
        menuFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        menuFrame.setSize(800, 400);
        menuFrame.setLocationRelativeTo(null);

        String[] columnNames = { "Name", "Price", "Ingredients" };
        String[][] data = new String[this.restaurant.getCurrentOrderItems().size()][3]; ///this.restaurant.getRestaurantMenu().size()

        if (data.length > 0 && data[0].length > 0) {
            int index = 0;
            for (int rowIndex = 0; rowIndex < data.length; rowIndex++) {
                for (int columnIndex = 0; columnIndex < data[0].length; columnIndex++) {

                    switch (columnIndex) {
                        case 0:
                            data[rowIndex][columnIndex] = this.restaurant.getCurrentOrderItems().get(index).getName();
                            break;
                        case 1:
                            data[rowIndex][columnIndex] = String.valueOf(this.restaurant.getCurrentOrderItems().get(index).getPrice());
                            break;
                        case 2:
                            if (this.restaurant.getCurrentOrderItems().get(index) instanceof CompositeProduct) {
                                String ingredients = "";
                                for (MenuItem menuItem: ((CompositeProduct) this.restaurant.getCurrentOrderItems().get(index)).getIngredients())
                                    ingredients += menuItem.getName() + ", ";
                                data[rowIndex][columnIndex] = ingredients;
                            }
                            else
                                data[rowIndex][columnIndex] = null;
                            break;
                        default:
                            data[rowIndex][columnIndex] = null;
                    }
                }
                index++;
            }
        }

        JTable menuTable = new JTable(data, columnNames);
        menuTable.setBounds(30, 40, 900, 300);
        JScrollPane scrollPane = new JScrollPane(menuTable);

        menuFrame.add(scrollPane);
        menuFrame.setResizable(false);
        menuFrame.setVisible(true);
    }

    private void allOrdersTableActionPerformed(java.awt.event.ActionEvent evt) {

        JFrame menuFrame = new JFrame("All Orders");
        menuFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        menuFrame.setSize(1600, 400);
        menuFrame.setLocationRelativeTo(null);

        String[] columnNames = { "Order ID", "Order Date", "Order Table", "Ordered Items", "Total Cost"};
        String[][] data = new String[this.restaurant.getRestaurantOrders().size()][5];

        Iterator hashMapIterator = this.restaurant.getRestaurantOrders().entrySet().iterator();

        if (data.length > 0 && data[0].length > 0) {
            for (int rowIndex = 0; rowIndex < data.length; rowIndex++) {

                if (hashMapIterator.hasNext()) {

                    Map.Entry mapElement = (Map.Entry)hashMapIterator.next();
                    Order order = (Order) mapElement.getKey();
                    ArrayList<MenuItem> orderItems = (ArrayList<MenuItem>) mapElement.getValue();

                    for (int columnIndex = 0; columnIndex < data[0].length; columnIndex++) {

                            switch (columnIndex) {
                                case 0:
                                    data[rowIndex][columnIndex] = String.valueOf(order.getId());
                                    break;
                                case 1:
                                    data[rowIndex][columnIndex] = String.valueOf(order.getDate());
                                    break;
                                case 2:
                                    data[rowIndex][columnIndex] = String.valueOf(order.getTable());
                                    break;
                                case 3:
                                    String orderedItems = "";
                                    for (MenuItem orderItem: orderItems) {
                                        orderedItems += orderItem.getName();
                                        orderedItems += ",";
                                    }
                                    data[rowIndex][columnIndex] = orderedItems;
                                    break;
                                case 4:
                                    data[rowIndex][columnIndex] = String.valueOf(this.getRestaurant().totalOrderCost(orderItems));
                                    break;
                                default:
                                    data[rowIndex][columnIndex] = null;
                            }
                    }
                }
            }
        }

        JTable menuTable = new JTable(data, columnNames);
        menuTable.setBounds(30, 40, 900, 300);
        JScrollPane scrollPane = new JScrollPane(menuTable);

        menuFrame.add(scrollPane);
        menuFrame.setResizable(false);
        menuFrame.setVisible(true);
    }

    private void addCurrentOrderActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if ( this.getRestaurant().getCurrentOrderItems().isEmpty() )
                JOptionPane.showMessageDialog(null, "No ordered items in order to add the order!", "No ordered items", JOptionPane.INFORMATION_MESSAGE);
            else {
                this.getRestaurant().setOrderCounter(this.getRestaurant().getOrderCounter() + 1);
                int numberOfTheTable = Integer.parseInt(tableNumberTextField.getText());
                if (tableNumberTextField.getText().isEmpty())
                    JOptionPane.showMessageDialog(null, "Please introduce the table number!", "Table number error", JOptionPane.ERROR_MESSAGE);
                else {
                    Order newOrder = new Order(this.getRestaurant().getOrderCounter(), new Date(), numberOfTheTable);
                    this.getRestaurant().getOrdersArray().add(newOrder);
                    this.getRestaurant().createNewOrder(newOrder, this.getRestaurant().getCurrentOrderItems());
                    JOptionPane.showMessageDialog(null, "Order added!", "Order", JOptionPane.INFORMATION_MESSAGE);
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Some error occured", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
