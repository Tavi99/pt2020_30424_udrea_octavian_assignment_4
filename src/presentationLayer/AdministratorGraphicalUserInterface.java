package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;

public class AdministratorGraphicalUserInterface {

    private Restaurant restaurant;

    private JFrame frame = new JFrame("Administrator Window");

    private JPanel centralPanel = new JPanel();
    private JPanel baseProductPanel = new JPanel();
    private JPanel baseProductOperationsPanel = new JPanel();
    private JPanel compositeProductPanel = new JPanel();
    private JPanel compositeProductOperationsPanel = new JPanel();

    private JTextField baseProductNameTextField = new JTextField();
    private JTextField baseProductPriceTextField = new JTextField();
    private JTextField newBaseProductPriceTextField = new JTextField();
    private JTextField compositeProductNameTextField = new JTextField();
    private JTextField compositeProductPriceTextField = new JTextField();
    private JTextField newCompositeProductNameTextField = new JTextField();
    private JTextField baseProductToRemoveFromCompositeProductTextField = new JTextField();

    private JLabel baseProductNameLabel = new JLabel("Base product name: ");
    private JLabel baseProductPriceLabel = new JLabel("Base product price: ");
    private JLabel newBaseProductPriceLabel = new JLabel("New base product price: ");
    private JLabel compositeProductNameLabel = new JLabel("Composite product name: ");
    private JLabel compositeProductPriceLabel = new JLabel("Composite product price: ");
    private JLabel newCompositeProductNameLabel = new JLabel("New composite product name: ");
    private JLabel baseProductToRemoveFromCompositeProductLabel = new JLabel("Base product to remove from composite: ");

    private JButton addBaseProductButton = new JButton("Add base product");
    private JButton deleteBaseProductButton = new JButton("Delete base product");
    private JButton modifyBaseProductPriceButton = new JButton("Modify base product price");
    private JButton addCompositeProductButton = new JButton("Add composite product");
    private JButton deleteCompositeProductButton = new JButton("Delete composite product");
    private JButton modifyCompositeProductNameButton = new JButton("Modify composite product name");
    private JButton removeBaseFromCompositeButton = new JButton("Remove base from composite");
    private JButton addBaseToCompositeProductButton = new JButton("Add base to composite");
    private JButton showMenuItemsButton = new JButton("Show menu");

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public AdministratorGraphicalUserInterface(Restaurant restaurant) {

        baseProductNameLabel.setHorizontalAlignment(JLabel.CENTER);
        baseProductPriceLabel.setHorizontalAlignment(JLabel.CENTER);
        compositeProductNameLabel.setHorizontalAlignment(JLabel.CENTER);
        compositeProductPriceLabel.setHorizontalAlignment(JLabel.CENTER);
        newBaseProductPriceLabel.setHorizontalAlignment(JLabel.CENTER);
        newCompositeProductNameLabel.setHorizontalAlignment(JLabel.CENTER);
        baseProductToRemoveFromCompositeProductLabel.setHorizontalAlignment(JLabel.CENTER);

        baseProductNameLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        baseProductPriceLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        compositeProductNameLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        compositeProductPriceLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        newBaseProductPriceLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        newCompositeProductNameLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        baseProductToRemoveFromCompositeProductLabel.setFont(new Font("Arial", Font.PLAIN, 10));

        baseProductNameTextField.setHorizontalAlignment(JTextField.CENTER);
        baseProductPriceTextField.setHorizontalAlignment(JTextField.CENTER);
        newBaseProductPriceTextField.setHorizontalAlignment(JTextField.CENTER);
        compositeProductNameTextField.setHorizontalAlignment(JTextField.CENTER);
        compositeProductPriceTextField.setHorizontalAlignment(JTextField.CENTER);
        newCompositeProductNameTextField.setHorizontalAlignment(JTextField.CENTER);
        baseProductToRemoveFromCompositeProductTextField.setHorizontalAlignment(JTextField.CENTER);

        baseProductNameTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        baseProductPriceTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        newBaseProductPriceLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        compositeProductNameTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        compositeProductPriceTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        newCompositeProductNameTextField.setFont(new Font("Arial", Font.PLAIN, 12));
        baseProductToRemoveFromCompositeProductTextField.setFont(new Font("Arial", Font.PLAIN, 12));

        baseProductNameTextField.setPreferredSize(new Dimension(50, 20));
        baseProductNameTextField.setEditable(true);
        baseProductPriceTextField.setPreferredSize(new Dimension(50, 20));
        baseProductPriceTextField.setEditable(true);
        newBaseProductPriceTextField.setPreferredSize(new Dimension(50, 20));
        newBaseProductPriceTextField.setEditable(true);
        compositeProductNameTextField.setPreferredSize(new Dimension(50, 20));
        compositeProductNameTextField.setEditable(true);
        compositeProductPriceTextField.setPreferredSize(new Dimension(50, 20));
        compositeProductPriceTextField.setEditable(true);
        newCompositeProductNameTextField.setPreferredSize(new Dimension(50, 20));
        newCompositeProductNameTextField.setEditable(true);
        baseProductToRemoveFromCompositeProductTextField.setPreferredSize(new Dimension(50, 20));
        baseProductToRemoveFromCompositeProductTextField.setEditable(true);

        addBaseProductButton.setFont(new Font("Arial", Font.PLAIN, 12));
        deleteBaseProductButton.setFont(new Font("Arial", Font.PLAIN, 12));
        modifyBaseProductPriceButton.setFont(new Font("Arial", Font.PLAIN, 12));
        addCompositeProductButton.setFont(new Font("Arial", Font.PLAIN, 12));
        deleteCompositeProductButton.setFont(new Font("Arial", Font.PLAIN, 12));
        modifyCompositeProductNameButton.setFont(new Font("Arial", Font.PLAIN, 12));
        removeBaseFromCompositeButton.setFont(new Font("Arial", Font.PLAIN, 12));
        addBaseToCompositeProductButton.setFont(new Font("Arial", Font.PLAIN, 12));
        showMenuItemsButton.setFont(new Font("Arial", Font.PLAIN, 12));

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 400);
        frame.setLocationRelativeTo(null);

        centralPanel.setLayout(new GridLayout(5, 1));
        centralPanel.setBackground(Color.WHITE);
        centralPanel.setVisible(true);

        addBaseProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBaseProductActionPerformed(evt);
            }
        });

        deleteBaseProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBaseProductActionPerformed(evt);
            }
        });

        modifyBaseProductPriceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyBaseProductPriceActionPerformed(evt);
            }
        });

        addCompositeProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCompositeProductActionPerformed(evt);
            }
        });

        deleteCompositeProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteCompositeProductActionPerformed(evt);
            }
        });

        modifyCompositeProductNameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyCompositeProductNameActionPerformed(evt);
            }
        });

        removeBaseFromCompositeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeBaseFromCompositeActionPerformed(evt);
            }
        });

        addBaseToCompositeProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBaseToCompositeProductActionPerformed(evt);
            }
        });

        showMenuItemsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showMenuItemsActionPerformed(evt);
            }
        });

        baseProductPanel.setLayout(new GridLayout(2, 4));
        baseProductOperationsPanel.setLayout(new FlowLayout());
        compositeProductPanel.setLayout(new GridLayout(2, 4));
        compositeProductOperationsPanel.setLayout(new FlowLayout());

        baseProductPanel.add(baseProductNameLabel);
        baseProductPanel.add(baseProductPriceLabel);
        baseProductPanel.add(newBaseProductPriceLabel);
        baseProductPanel.add(baseProductNameTextField);
        baseProductPanel.add(baseProductPriceTextField);
        baseProductPanel.add(newBaseProductPriceTextField);

        baseProductOperationsPanel.add(addBaseProductButton);
        baseProductOperationsPanel.add(deleteBaseProductButton);
        baseProductOperationsPanel.add(modifyBaseProductPriceButton);

        compositeProductPanel.add(compositeProductNameLabel);
        compositeProductPanel.add(compositeProductNameTextField);
        compositeProductPanel.add(compositeProductPriceLabel);
        compositeProductPanel.add(compositeProductPriceTextField);
        compositeProductPanel.add(newCompositeProductNameLabel);
        compositeProductPanel.add(newCompositeProductNameTextField);
        compositeProductPanel.add(baseProductToRemoveFromCompositeProductLabel);
        compositeProductPanel.add(baseProductToRemoveFromCompositeProductTextField);

        compositeProductOperationsPanel.add(addCompositeProductButton);
        compositeProductOperationsPanel.add(deleteCompositeProductButton);
        compositeProductOperationsPanel.add(modifyCompositeProductNameButton);
        compositeProductOperationsPanel.add(removeBaseFromCompositeButton);
        compositeProductOperationsPanel.add(addBaseToCompositeProductButton);

        centralPanel.add(baseProductPanel);
        centralPanel.add(baseProductOperationsPanel);
        centralPanel.add(compositeProductPanel);
        centralPanel.add(compositeProductOperationsPanel);
        centralPanel.add(showMenuItemsButton);

        frame.add(centralPanel);
        frame.setResizable(false);
        frame.setVisible(true);

        this.restaurant = restaurant;
    }

    private void addBaseProductActionPerformed(java.awt.event.ActionEvent evt) {

        try {
            MenuItem baseProduct = new BaseProduct();
            baseProduct.setName(baseProductNameTextField.getText());
            baseProduct.setPrice(Double.parseDouble(baseProductPriceTextField.getText()));

            if (this.getRestaurant().getRestaurantMenu().contains(baseProduct) || this.restaurant.searchItemByName(baseProduct.getName()) != null)
                JOptionPane.showMessageDialog(null, "Product already exists in menu!", "Base product info", JOptionPane.INFORMATION_MESSAGE);
            else
                this.getRestaurant().createNewMenuItem(baseProduct);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void deleteBaseProductActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem baseProduct = new BaseProduct();
            baseProduct.setName(baseProductNameTextField.getText());
            MenuItem itemFound = this.restaurant.searchItemByName(baseProduct.getName());
            if (itemFound != null) {
                for (MenuItem menuItem: this.getRestaurant().getRestaurantMenu())
                    if (menuItem instanceof CompositeProduct)
                        if (((CompositeProduct) menuItem).getIngredients().contains(itemFound) == true) {
                            ((CompositeProduct) menuItem).getIngredients().remove(itemFound);
                            menuItem.setPrice(menuItem.getPrice() - itemFound.getPrice());
                        }

                this.getRestaurant().deleteMenuItem(restaurant.getMenuItemIndex(itemFound));
            }
            else
                JOptionPane.showMessageDialog(null, "Product does not exist in menu!", "Base product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void modifyBaseProductPriceActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem baseProduct = new BaseProduct();
            baseProduct.setName(baseProductNameTextField.getText());
            MenuItem itemFound = this.restaurant.searchItemByName(baseProduct.getName());
            double newPrice = Double.parseDouble(newBaseProductPriceTextField.getText());
            if (itemFound != null) {
                for (MenuItem menuItem: this.getRestaurant().getRestaurantMenu())
                    if (menuItem instanceof CompositeProduct)
                        if (((CompositeProduct) menuItem).getIngredients().contains(itemFound) == true) {
                            menuItem.setPrice(menuItem.getPrice() - itemFound.getPrice() + newPrice);
                        }
                this.getRestaurant().editMenuItem(itemFound, newPrice);
            } else
                JOptionPane.showMessageDialog(null, "Product does not exist in menu!", "Base product info", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/new price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void addCompositeProductActionPerformed(java.awt.event.ActionEvent evt) {

        try {
            MenuItem compositeProduct = new CompositeProduct();
            compositeProduct.setName(compositeProductNameTextField.getText());
            compositeProduct.setPrice(Double.parseDouble(compositeProductPriceTextField.getText()));

            if (this.restaurant.searchItemByName(compositeProduct.getName()) == null)
                this.getRestaurant().createNewMenuItem(compositeProduct);
            else
                JOptionPane.showMessageDialog(null, "Product already exists in menu!", "Composite product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void deleteCompositeProductActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem compositeProduct = new CompositeProduct();
            compositeProduct.setName(compositeProductNameTextField.getText());
            MenuItem itemFound = this.restaurant.searchItemByName(compositeProduct.getName());

            if (itemFound != null)
                this.getRestaurant().deleteMenuItem(this.restaurant.getMenuItemIndex(itemFound));
            else
                JOptionPane.showMessageDialog(null, "Product does not exist in menu!", "Composite product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void modifyCompositeProductNameActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem compositeProduct = new CompositeProduct();
            compositeProduct.setName(compositeProductNameTextField.getText());
            MenuItem itemFound = this.restaurant.searchItemByName(compositeProduct.getName());
            String newName = newCompositeProductNameTextField.getText();

            if (itemFound != null) {
                itemFound.setName(newName);
            } else
                JOptionPane.showMessageDialog(null, "Product does not exist in menu!", "Composite product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/new price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void removeBaseFromCompositeActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem baseProduct = new BaseProduct();
            baseProduct.setName(baseProductToRemoveFromCompositeProductTextField.getText());
            MenuItem baseItemFound = this.restaurant.searchItemByName(baseProduct.getName());

            MenuItem compositeProduct = new CompositeProduct();
            compositeProduct.setName(compositeProductNameTextField.getText());
            MenuItem compositeItemFound = this.restaurant.searchItemByName(compositeProduct.getName());

            if (baseItemFound != null && compositeItemFound != null) {
                ((CompositeProduct) compositeItemFound).getIngredients().remove(baseItemFound);
                compositeItemFound.setPrice(compositeItemFound.computePrice() - baseItemFound.computePrice());
            } else
                JOptionPane.showMessageDialog(null, "Base/Composite product does not exist in menu!", "Composite product info", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required base product name/composite product name", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void addBaseToCompositeProductActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            MenuItem baseProduct = new BaseProduct();
            baseProduct.setName(baseProductNameTextField.getText());
            MenuItem baseProductFound = this.restaurant.searchItemByName(baseProduct.getName());

            CompositeProduct compositeProduct = new CompositeProduct();
            compositeProduct.setName(compositeProductNameTextField.getText());
            MenuItem compositeProductFound = this.restaurant.searchItemByName(compositeProduct.getName());

            if (baseProductFound == null || compositeProductFound == null)
                JOptionPane.showMessageDialog(null, "Base or Composite product does not exist in menu!", "Product info", JOptionPane.INFORMATION_MESSAGE);
            else {

                if ( ((CompositeProduct) compositeProductFound).getIngredients().contains(baseProductFound) == true) {
                    JOptionPane.showMessageDialog(null, "Composite product already has this item in composition!", "Composite product info", JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                    ((CompositeProduct) compositeProductFound).getIngredients().add(baseProductFound);
                    compositeProductFound.setPrice(compositeProductFound.computePrice() + baseProductFound.computePrice());
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Required name/price", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showMenuItemsActionPerformed(java.awt.event.ActionEvent evt) {
        JFrame menuFrame = new JFrame("Restaurant Menu");
        menuFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        menuFrame.setSize(800, 400);
        menuFrame.setLocationRelativeTo(null);

        String[] columnNames = { "Name", "Price", "Ingredients" };
        String[][] data = new String[this.restaurant.getRestaurantMenu().size()][3];

        if (data.length > 0 && data[0].length > 0) {
            int index = 0;
            for (int rowIndex = 0; rowIndex < data.length; rowIndex++) {
                for (int columnIndex = 0; columnIndex < data[0].length; columnIndex++) {

                    switch (columnIndex) {
                        case 0:
                            data[rowIndex][columnIndex] = this.restaurant.getRestaurantMenu().get(index).getName();
                            break;
                        case 1:
                            data[rowIndex][columnIndex] = String.valueOf(this.restaurant.getRestaurantMenu().get(index).computePrice());
                            break;
                        case 2:
                            if (this.restaurant.getRestaurantMenu().get(index) instanceof CompositeProduct) {
                                String ingredients = "";
                                for (MenuItem menuItem: ((CompositeProduct) this.restaurant.getRestaurantMenu().get(index)).getIngredients())
                                    ingredients += menuItem.getName() + ", ";
                                data[rowIndex][columnIndex] = ingredients;
                            }
                            else
                                data[rowIndex][columnIndex] = null;
                            break;
                        default:
                            data[rowIndex][columnIndex] = null;
                    }
                }
                index++;
            }
        }

        JTable menuTable = new JTable(data, columnNames);
        menuTable.setBounds(30, 40, 900, 300);
        JScrollPane scrollPane = new JScrollPane(menuTable);

        menuFrame.add(scrollPane);
        menuFrame.setResizable(false);
        menuFrame.setVisible(true);
    }

}
